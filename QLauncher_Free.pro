#-------------------------------------------------
#
# Project created by QtCreator 2014-07-20T00:33:42
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QLauncher_Free
TEMPLATE = app


SOURCES += main.cpp \
    loginform.cpp \
    TinyAES/tinyaes.cpp \
    Settings.cpp

HEADERS  += \
    Settings.h \
    loginform.h \
    tinyaes.h \
    TinyAES/tinyaes.h

FORMS    += \
    loginform.ui

RESOURCES +=

