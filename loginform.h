#ifndef LOGINFORM_H
#define LOGINFORM_H

#include "Settings.h"

namespace Ui {
class LoginForm;
}

class LoginForm : public QWidget
{
    Q_OBJECT


    QString getpass();
    QNetworkAccessManager *manager;
    QNetworkRequest request;
    QNetworkReply *reply;
    QNetworkReply *replyauth;
    TinyAES crypto;
public:
    explicit LoginForm(QWidget *parent = 0);
    ~LoginForm();

private:
    Ui::LoginForm *ui;
    QWidget Downloading;
public:
    void downloadClient(QString server);


public slots:
    void auth();
    void serverfinished();
    void login();


};

#endif // LOGINFORM_H
