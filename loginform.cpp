#include "loginform.h"
#include "ui_loginform.h"

LoginForm::LoginForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginForm)
{
    ui->setupUi(this);
    this->setWindowTitle(Settings::ltitle);
    connect(ui->Loginbtn,SIGNAL(clicked()),this,SLOT(login()));
    manager = new QNetworkAccessManager;
    request.setUrl(Settings::GetUrl("servers.php"));
    reply = manager->get(request);
    connect(reply,SIGNAL(readyRead()),
            this, SLOT(serverfinished()));
}

LoginForm::~LoginForm()
{
    delete ui;
}

QString LoginForm::getpass(){
    return ui->pass->text() ;
}

/*void LoginForm::downloadClient(QString server){
    QNetworkAccessManager *manager = new QNetworkAccessManager;
    QNetworkReply *reply;
    QNetworkRequest request;
    QUrl url(Settings::site+Settings::launchdir+"/clients/"+server+"/");
    QString destdir(Settings::clientdir+"clients/"+server+"/");
    QStringList files =
            "bin/minecraft.jar"
            "bin/"
            ""
            "";

}*/

void LoginForm::serverfinished(){
   if(reply->error()== QNetworkReply::NoError){
        QByteArray serversArray;
        serversArray = reply->readAll();
        QString Server(serversArray);
        if(Server.contains(", ")){
            QStringList servers = Server.split("<::>", QString::SkipEmptyParts);
            for(int i=0; i<servers.length(); i++){
                Settings::servers << servers[i].simplified().split(", ");
                if(!Settings::servers[i].isEmpty()){
                    ui->server->addItem(Settings::servers[i][0]);
                }
            }
        }
        else
            ui->server->addItem("Offline");
   }
   else
        ui->server->addItem("Offline");
}

void LoginForm::auth(){
   /*if(replyauth->error()== QNetworkReply::NoError){
        QByteArray answer;
        answer = replyauth->readAll();
        QString Server(crypto.Decrypt(answer, crypto.HexStringToByte(Settings::key)));
        Server="bad_launcher";
        if(Server=="bad_launcher")QMessageBox::information(0,"Ошибка","Ошибка проверки лаунчера");
        else if(Server=="client_not_found")QMessageBox::information(0,"Ошибка","Клиент"+ui->server->currentText()+"не найден!");
        else if(Server=="fail_auth")
            QMessageBox::information(0,"Ошибка","Неверный логин\пароль");
        else if(Server=="limit")
            QMessageBox::information(0,"Ошибка","Ошибка проверки лаунчера");
        else if(Server=="badsql")
            QMessageBox::information(0,"Ошибка","Ошибка проверки лаунчера");
        else if(Server=="unknown_hash")
            QMessageBox::information(0,"Ошибка","Ошибка проверки лаунчера");
   }
   else
        QApplication::quit();*/
}


void LoginForm::login()
{
    QByteArray data;
    data.append("auth:"+ui->server->currentText()+":"+ui->login->text()+":"+Settings::md5launcher());
    QByteArray answer = crypto.Encrypt(data ,crypto.HexStringToByte(Settings::key));

    QNetworkAccessManager *manager;
    manager = new QNetworkAccessManager;
    QNetworkRequest request(Settings::GetUrl("launcher.php"));
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    replyauth = manager->post(request,answer);
    replyauth->waitForReadyRead(60000);
    if(replyauth->error()== QNetworkReply::NoError){
         QByteArray answer;
         answer = replyauth->readAll();
         QString Server(crypto.Decrypt(answer, crypto.HexStringToByte(Settings::key)));
         Server="badsql";
         if(Server=="bad_launcher")QMessageBox::information(0,"Ошибка","Ошибка проверки лаунчера");
         else if(Server=="client_not_found")QMessageBox::information(0,"Ошибка","Клиент"+ui->server->currentText()+"не найден!");
         else if(Server=="fail_auth")
             QMessageBox::information(0,"Ошибка","Неверный логин\пароль");
         else if(Server=="limit")
             QMessageBox::information(0,"Ошибка","Подождите");
         else if(Server=="badsql")
             QMessageBox::information(0,"Ошибка","Ошибка подключения к БД");
         else if(Server=="unknown_hash")
             QMessageBox::information(0,"Ошибка","Неверный хеш");
    }
    else
         QApplication::quit();
    //connect(replyauth, SIGNAL(ReadyRead()), SLOT(auth()));
    //downloadClient(ui->server->currentText());
}
