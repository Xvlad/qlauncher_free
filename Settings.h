#ifndef SETTINGS_H
#define SETTINGS_H
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

#include <QApplication>
#include <QtCore>
#include <QString>
#include <QUrl>
#include <QVector>
#include <QMessageBox>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QHttpPart>
#include <QtNetwork>
#include <QUrl>
#include <QtNetwork/QNetworkAccessManager>
#include <QWidget>
#include "TinyAES\tinyaes.h"



namespace Settings{
    static const QString ltitle = "Qlauncher_Free"; // Заголовок
    static const QString site = "http://11qlauncher.esy.es/";       // Домен в виде xxx.yyy
    static const QString launchdir = "QLauncher";   // Папка лаунчера на сайте
    static const QUrl reglink("http://test.com/registration.http"); // Ссылка на регистрацию
    static const QString clientdir = QDir::homePath()+"/Qlauncher/"; // Папка лаунчера Windows - %HOMEPATH%/dir; Linux - ~/dir, OS X - ~/dir
    static const QString key("587b14e9ef30e95b64dc5ec71230197a");
    static QVector<QStringList> servers;


    QUrl GetUrl(QString script);
    QByteArray md5launcher();
}

#endif // SETTINGS_H
