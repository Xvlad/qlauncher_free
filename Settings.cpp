#include "Settings.h"

QUrl Settings::GetUrl(QString script){
        return QUrl(site+launchdir+"/"+script);
}

QByteArray Settings::md5launcher(){
    QFile program(QApplication::arguments()[0]);
    if(program.open(QIODevice::ReadOnly | QIODevice::Unbuffered)){
        QByteArray programdata = program.readAll();
        QCryptographicHash md5(QCryptographicHash::Md5);
        md5.addData(programdata);
        return md5.result().toHex();
    }
    else return NULL;

}
